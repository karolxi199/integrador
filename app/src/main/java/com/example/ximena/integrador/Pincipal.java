package com.example.ximena.integrador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class Pincipal extends AppCompatActivity {


 Button Credito, Pedido, Inventario, Cobros_cliente, Producto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Credito = (Button) findViewById(R.id.btnCredito);
        Pedido = (Button) findViewById(R.id.btnRegistarPedidos);
        Inventario = (Button)findViewById(R.id.btnRevisarInventario) ;
                Producto = (Button)findViewById(R.id.btnRegistrosProducto);

        Credito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pincipal.this, VentasCredito.class);
                startActivity(intent);
            }
        });
        Pedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inten = new Intent(Pincipal.this, RegistraPedido.class);
                startActivity(inten);
            }
        });
        Inventario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inventario = new Intent(Pincipal.this,RevisarInventario.class);
                startActivity(inventario);

            }
        });


        Producto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent productos = new Intent( Pincipal.this,Produccto.class);
                startActivity(productos);

            }
        });
    }
}
