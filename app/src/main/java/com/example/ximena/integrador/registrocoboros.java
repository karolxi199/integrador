package com.example.ximena.integrador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class registrocoboros extends AppCompatActivity {
Button Ccliente,Ncliente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrocoboros);
        Ccliente=(Button)findViewById(R.id.btnCobroscliente);
        Ncliente=(Button)findViewById(R.id.btnregistarnuevocliente);

        Ccliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(registrocoboros.this, cobrocliente.class);
                startActivity(intent);

            }
        });

        Ncliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(registrocoboros.this, CobrosCliente.class);
                startActivity(intent);

            }
        });
    }
}
