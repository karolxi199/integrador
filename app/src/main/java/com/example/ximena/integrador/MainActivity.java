package com.example.ximena.integrador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button Resven,Cobroos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pincipal);
        Resven =(Button)findViewById(R.id.btnCredito);
        Cobroos=(Button)findViewById(R.id.btnCobros);

        Resven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Pincipal.class);
                startActivity(intent);

            }
        });
        Cobroos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intt = new Intent(MainActivity.this, registrocoboros.class);
                startActivity(intt);
            }
        });
    }
}
